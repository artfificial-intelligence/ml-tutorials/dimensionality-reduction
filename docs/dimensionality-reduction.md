# 차원 축소: PCA와 t-SNE

## <a name="intro"></a> 개요
이 포스팅에서는 일반적으로 사용되는 두 가지 기법인 주성분 분석(PCA)과 t-분산 확률적 이웃 임베딩(t-SNE)을 사용하여 데이터의 차원을 줄이는 방법을 설명한다. 차원 축소는 고차원 데이터를 저차원 표현으로 변환하면서 관련 정보를 최대한 보존하는 과정이다. 차원 축소는 다음과 같은 작업에 도움을 줄 수 있다.

- 고차원 데이터를 2차원 또는 3차원 공간으로 시각화
- 기계 학습 알고리즘의 계산 비용과 복잡성 감소
- 데이터에서 노이즈와 중복 제거
- 데이터의 해석 가능성과 이해 향상

PCA와 t-SNE는 가장 널리 사용되는 차원 축소 기법이지만 서로 다른 장단점을 가지고 있다. PCA는 데이터에서 최대 분산의 방향을 찾고, 데이터를 저차원 부분 공간에 투영하려는 선형 기법이다. t-SNE는 비선형 기법으로, 고차원 공간에서 유사한 데이터 점의 클러스터를 찾고, 데이터의 로컬 구조를 보존하면서 저차원 공간으로 매핑하려고 한다.

이 포스팅에서는 PCA와 t-SNE를 Python과 scikit-learn 라이브러리를 사용하여 서로 다른 데이터세트에 적용하는 방법을 배울 것이다. 또한 PCA와 t-SNE의 결과를 비교하고 각 기법의 장단점에 대하여도 배울 것이다.

## <a name="sec_02"></a> 차원 축소란 무엇이며 왜 중요할까?
차원 축소는 관련 정보를 최대한 보존하면서 고차원 데이터를 저차원 표현으로 변환하는 과정이다. 고차원 데이터는 이미지, 텍스트 또는 센서 데이터와 같이 많은 특징 또는 변수를 갖는 데이터를 의미한다. 예를 들어 100 x 100 픽셀의 이미지는 10,000 차원의 벡터로 표현될 수 있으며, 여기서 각 차원은 각 픽셀 값에 해당한다.

차원 축소가 왜 중요 할까? 데이터의 차원을 축소하려는 이유는 다음과 같다.

- **시각화**: 인간은 3차원까지만 인식할 수 있으므로 고차원 데이터를 시각화하는 것은 어렵다. 차원 축소는 데이터를 2차원 또는 3차원 공간에 투영하고, 여기서 데이터의 패턴과 관계를 더 쉽게 탐색할 수 있도록 도와준다.
- **계산**: 특히 기계 학습 알고리즘의 경우 고차원 데이터는 계산 비용이 많이 들고 처리하기가 복잡할 수 있다. 차원 축소는 데이터의 크기와 복잡성을 줄이는 데 도움이 되어 알고리즘의 속도와 성능을 향상시킬 수 있다.
- **노이즈와 중복성**: 고차원 데이터는 많은 노이즈와 중복성을 포함할 수 있으며, 이는 데이터 분석의 품질과 정확성에 영향을 미칠 수 있다. 차원 감소는 데이터에서 관련 없는 중복된 특징을 제거하는 데 도움이 될 수 있으며, 이는 신호 대 노이즈 비율과 데이터의 해석 가능성을 향상시킬 수 있다.

차원 축소를 위한 많은 기술들이 있지만, 선형과 비선형의 두 가지 범주로 크게 분류할 수 있다. 선형 기술은 데이터가 선형 부분 공간 위 또는 근처에 있다고 가정하고, 데이터를 저차원 공간으로 최적의 선형 투영을 찾으려 한다. 비선형 기술은 데이터 구조에 대해 어떤 가정도 하지 않으며, 데이터를 저차원 공간으로 비선형 매핑하는 것을 찾으려 한다. 이 튜토리얼에서는 각 범주에서 두 가지 인기 있는 기술인 주성분 분석(PCA)과 t-분산 확률적 이웃 임베딩(t-SNE)에 대해 설명한다.

## <a name="sec_03"></a> 주성분 분석(PCA)
주성분 분석(Principle Component Analysis, PCA)은 데이터에서 최대 분산의 방향을 찾아 데이터를 저차원 부분공간에 투영하는 선형 차원 축소 기법이다. PCA는 원 데이터와 투영 데이터 사이의 재구성 오차를 최소화하여 데이터의 최적 선형 근사치를 찾는 방법이라고 볼 수 있다.

PCA의 주요 단계는 다음과 같다.

1. 평균과 단위 분산이 0이 되도록 데이터를 표준화한다.
1. 피처 쌍 간의 상관 관계를 측정하는 데이터의 공분산 행렬을 계산한다.
1. 주성분의 크기와 방향을 나타내는 공분산 행렬의 고유값(eigenvlaue)과 고유벡터(eigen vector)를 계산한다.
1. 고유값을 내림차순으로 정렬하고 k개의 가장 큰 고유값에 해당하는 상위 k개의 고유벡터를 선택한다. 여기서 k는 축소하고자 하는 데이터의 차원이다.
1. 데이터 행렬과 고유 벡터 행렬을 곱하여 원래 데이터를 k개의 고유 벡터에 걸쳐 있는 새로운 부분 공간으로 변환한다.

Python에서는 scikit-learn 라이브러리를 사용하여 데이터에 대한 PCA를 수행할 수 있다. 다음 코드는 `sklearn.decomposition` 모듈에서 PCA 클래스를 임포트하는 방법과 `fit_transform` 메서드를 사용하여 데이터를 맞추고 변환하는 방법을 보여준다.

```python
# Import the PCA class
from sklearn.decomposition import PCA

# Create a PCA object with the desired number of components
pca = PCA(n_components=2)
# Fit and transform the data
data_reduced = pca.fit_transform(data)
```

`data_reduced` 변수에는 2차원으로 축소된 데이터가 저장된다. 각 구성요소 별로 데이터의 전체 분산을 어느 정도 설명하는 지를 보여주는 `pca` 객체의 `explained_variance_ratio_attribute`를 이용하여 각 구성요소의 설명된 분산 비율을 접근할 수도 있다.

## <a name="sec_04"></a> t-분산 확률적 이웃 임베딩(t-SNE)
t-SNE(t-Distributed Stochastic Neighbor Embedding)는 비선형 차원 축소 기법으로, 고차원 공간에서 유사한 데이터 포인트의 클러스터를 찾아내고, 데이터의 로컬 구조를 보존하면서 저차원 공간에 매핑하려는 기법이다. t-SNE는 원래 공간과 축소 공간에서 데이터 포인트의 확률 분포 간의 편차를 최소화하여 데이터의 최적 비선형 근사치를 찾는 방법이라고 볼 수 있다.

t-SNE의 주요 단계는 다음과 같다.

1. 한 점이 다른 점의 이웃일 확률을 측정하기 위해 가우시안 커널을 사용하여 고차원 공간에서 두 데이터 점들 간의 유사성을 계산한다.
1. 한 점이 다른 점의 이웃일 확률을 측정하기 위해 Student's t-distribution kernel을 사용하여 저차원 공간에 있는 데이터 두 점들 간의 유사성을 계산한다.
1. 저차원 공간에서 데이터 포인트의 위치를 조정하기 위해 경사 하강 알고리즘을 사용하여 두 확률 분포 사이의 Kullback-Leibler 발산을 최소화한다.

Python에서는 scikit-learn 라이브러리를 사용하여 데이터에 대한 t-SNE를 수행할 수 있다. 다음 코드는 sklearn.manifold 모듈에서 TSNE 클래스를 임포트하고 `fit_transform` 메서드를 사용하여 데이터를 맞추고 변환하는 방법을 보인다.

```python
# Import the TSNE class
from sklearn.manifold import TSNE

# Create a TSNE object with the desired parameters
tsne = TSNE(n_components=2, perplexity=30, learning_rate=200, n_iter=1000)
# Fit and transform the data
data_reduced = tsne.fit_transform(data)
```

`data_reduced` 변수는 2차원으로 축소된 데이터를 저장한다. 또한 `tsne` 객체의 `kl_divergence_attribute`를 사용하여 최종 KL 발산 값을 접근할 수 있으며, 이는 축소된 데이터가 원래 데이터에 얼마나 잘 근접하는 지를 보인다.

## <a name="sec_05"></a> 데이터세트에서 PCA와 t-SNE의 비교
이 절에서는 PCA와 t-SNE가 서로 다른 데이터 세트에서 어떻게 작동하는지 살펴보고 그 결과를 비교할 것이다. scikit-learn 라이브러리에서 세 가지 데이터 세트 [iris 데이터세트](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_iris.html), [digits 데이터세트](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_digits.html)와 [face 데이터세트](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.fetch_olivetti_faces.html)를 사용할 것이다. iris 데이터세트에는 sepal length, sepal width, petal length와 petal width 네 가지 피처가 있는 다른 세 종류의 iris 꽃 샘플 150개가 있다. digits 데이터세트에는 0부터 9까지 손으로 쓴 숫자 1797개의 이미지가 있으며, 64개의 피처은 8 x 8 이미지의 픽셀 값이다. 얼굴 데이터세트는 4096개 피처로 64 x 64 이미지의 픽셀 값이다. 각 데이터세트에 대한 메타 정보를 다음과 같이 볼 수 있다.

```python
from sklearn.datasets import load_iris, load_digits, fetch_olivetti_faces

# Load iris datasets
iris = load_iris()
print(iris.DESCR)    # Print the description of the dataset
# Load digits datasets
digits = load_digits()
print(digits.DESCR)    # Print the description of the dataset
# load 
faces = fetch_olivetti_faces()
print(faces.DESCR)    # Print the description of the dataset
```

출력은 다음과 같다.

```
.. _iris_dataset:

Iris plants dataset
--------------------

**Data Set Characteristics:**

:Number of Instances: 150 (50 in each of three classes)
:Number of Attributes: 4 numeric, predictive attributes and the class
:Attribute Information:
    - sepal length in cm
    - sepal width in cm
    - petal length in cm
    - petal width in cm
    - class:
            - Iris-Setosa
            - Iris-Versicolour
            - Iris-Virginica

:Summary Statistics:

============== ==== ==== ======= ===== ====================
                Min  Max   Mean    SD   Class Correlation
============== ==== ==== ======= ===== ====================
sepal length:   4.3  7.9   5.84   0.83    0.7826
sepal width:    2.0  4.4   3.05   0.43   -0.4194
petal length:   1.0  6.9   3.76   1.76    0.9490  (high!)
petal width:    0.1  2.5   1.20   0.76    0.9565  (high!)
============== ==== ==== ======= ===== ====================

:Missing Attribute Values: None
:Class Distribution: 33.3% for each of 3 classes.
:Creator: R.A. Fisher
:Donor: Michael Marshall (MARSHALL%PLU@io.arc.nasa.gov)
:Date: July, 1988

The famous Iris database, first used by Sir R.A. Fisher. The dataset is taken
from Fisher's paper. Note that it's the same as in R, but not as in the UCI
Machine Learning Repository, which has two wrong data points.

This is perhaps the best known database to be found in the
pattern recognition literature.  Fisher's paper is a classic in the field and
is referenced frequently to this day.  (See Duda & Hart, for example.)  The
data set contains 3 classes of 50 instances each, where each class refers to a
type of iris plant.  One class is linearly separable from the other 2; the
latter are NOT linearly separable from each other.

|details-start|
**References**
|details-split|

- Fisher, R.A. "The use of multiple measurements in taxonomic problems"
  Annual Eugenics, 7, Part II, 179-188 (1936); also in "Contributions to
  Mathematical Statistics" (John Wiley, NY, 1950).
- Duda, R.O., & Hart, P.E. (1973) Pattern Classification and Scene Analysis.
  (Q327.D83) John Wiley & Sons.  ISBN 0-471-22361-1.  See page 218.
- Dasarathy, B.V. (1980) "Nosing Around the Neighborhood: A New System
  Structure and Classification Rule for Recognition in Partially Exposed
  Environments".  IEEE Transactions on Pattern Analysis and Machine
  Intelligence, Vol. PAMI-2, No. 1, 67-71.
- Gates, G.W. (1972) "The Reduced Nearest Neighbor Rule".  IEEE Transactions
  on Information Theory, May 1972, 431-433.
- See also: 1988 MLC Proceedings, 54-64.  Cheeseman et al"s AUTOCLASS II
  conceptual clustering system finds 3 classes in the data.
- Many, many more ...

|details-end|

.. _digits_dataset:

Optical recognition of handwritten digits dataset
--------------------------------------------------

**Data Set Characteristics:**

:Number of Instances: 1797
:Number of Attributes: 64
:Attribute Information: 8x8 image of integer pixels in the range 0..16.
:Missing Attribute Values: None
:Creator: E. Alpaydin (alpaydin '@' boun.edu.tr)
:Date: July; 1998

This is a copy of the test set of the UCI ML hand-written digits datasets
https://archive.ics.uci.edu/ml/datasets/Optical+Recognition+of+Handwritten+Digits

The data set contains images of hand-written digits: 10 classes where
each class refers to a digit.

Preprocessing programs made available by NIST were used to extract
normalized bitmaps of handwritten digits from a preprinted form. From a
total of 43 people, 30 contributed to the training set and different 13
to the test set. 32x32 bitmaps are divided into nonoverlapping blocks of
4x4 and the number of on pixels are counted in each block. This generates
an input matrix of 8x8 where each element is an integer in the range
0..16. This reduces dimensionality and gives invariance to small
distortions.

For info on NIST preprocessing routines, see M. D. Garris, J. L. Blue, G.
T. Candela, D. L. Dimmick, J. Geist, P. J. Grother, S. A. Janet, and C.
L. Wilson, NIST Form-Based Handprint Recognition System, NISTIR 5469,
1994.

|details-start|
**References**
|details-split|

- C. Kaynak (1995) Methods of Combining Multiple Classifiers and Their
  Applications to Handwritten Digit Recognition, MSc Thesis, Institute of
  Graduate Studies in Science and Engineering, Bogazici University.
- E. Alpaydin, C. Kaynak (1998) Cascading Classifiers, Kybernetika.
- Ken Tang and Ponnuthurai N. Suganthan and Xi Yao and A. Kai Qin.
  Linear dimensionalityreduction using relevance weighted LDA. School of
  Electrical and Electronic Engineering Nanyang Technological University.
  2005.
- Claudio Gentile. A New Approximate Maximal Margin Classification
  Algorithm. NIPS. 2000.

|details-end|

.. _olivetti_faces_dataset:

The Olivetti faces dataset
--------------------------

`This dataset contains a set of face images`_ taken between April 1992 and
April 1994 at AT&T Laboratories Cambridge. The
:func:`sklearn.datasets.fetch_olivetti_faces` function is the data
fetching / caching function that downloads the data
archive from AT&T.

.. _This dataset contains a set of face images: https://cam-orl.co.uk/facedatabase.html

As described on the original website:

    There are ten different images of each of 40 distinct subjects. For some
    subjects, the images were taken at different times, varying the lighting,
    facial expressions (open / closed eyes, smiling / not smiling) and facial
    details (glasses / no glasses). All the images were taken against a dark
    homogeneous background with the subjects in an upright, frontal position
    (with tolerance for some side movement).

**Data Set Characteristics:**

=================   =====================
Classes                                40
Samples total                         400
Dimensionality                       4096
Features            real, between 0 and 1
=================   =====================

The image is quantized to 256 grey levels and stored as unsigned 8-bit
integers; the loader will convert these to floating point values on the
interval [0, 1], which are easier to work with for many algorithms.

The "target" for this database is an integer from 0 to 39 indicating the
identity of the person pictured; however, with only 10 examples per class, this
relatively small dataset is more interesting from an unsupervised or
semi-supervised perspective.

The original dataset consisted of 92 x 112, while the version available here
consists of 64x64 images.

When using these images, please give credit to AT&T Laboratories Cambridge.

```

이전과 동일한 코드를 사용하여 각 데이터세트에 대해 PCA와 t-SNE를 수행한 다음 mathplotlib 라이브러리를 사용하여 축소된 데이터를 플롯할 수 있다. 데이터의 레이블을 사용하여 클래스 또는 범주에 따라 점을 색칠할 수도 있다. 다음 코드는 `sklean.datasets`와 `matplotlib.pyplot` 모듈에서 데이터 집합과 플롯 함수를 가져오는 방법과 plt.scatter 메서드를 사용하여 축소된 데이터를 플롯하는 방법을 보여준다.

```python
# Import the datasets and the plotting function
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris, load_digits, fetch_olivetti_faces

# Plot the reduced data
def scatter_plot_data(dataset):
    # Create a figure and an axis
    _, ax = plt.subplots()
    # Scatter the data points with different colors according to the labels
    scatter = ax.scatter(dataset.data[:, 0], dataset.data[:, 1], c=dataset.target)
    # Set the title and the axis labels
    ax.set(xlabel=dataset.feature_names[0], ylabel=dataset.feature_names[1])
    _ = ax.legend(
        scatter.legend_elements()[0], dataset.target_names, loc="lower right", title="Classes"
    )

    # ax.set_title(title)
    # ax.set_xlabel('Component 1')
    # ax.set_ylabel('Component 2')
    # Show the plot
    plt.show()
    return

# Load iris datasets
iris = load_iris()
scatter_plot_data(iris)
```

위의 코드 출력은 아래와 같다.

![](./images/iris_scatter.png)

이제 iris 데이터세트에서 PCA와 t-SNE가 어떻게 수행되는지 살펴보도록 한다. iris 데이터세트는 4차원이며, 두 가지 기법을 모두 사용하여 2차원으로 축소할 것이다. 다음 코드는 iris 데이터세트에서 PCA와 t-SNE를 수행하는 방법과 `scatter_plot_data` 함수를 사용하여 축소된 데이터를 플롯하는 방법을 보여준다.

```python
# Import the datasets and the plotting function
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris, load_digits, fetch_olivetti_faces
# Import the PCA and TSNE classes
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE


# Plot the reduced data
def scatter_plot_data(x, y, classes, title="Scatter plot", x_label="Component 1", y_label="Component 2"):
    # Create a figure and an axis
    _, ax = plt.subplots()
    # Scatter the data points with different colors according to the labels
    scatter = ax.scatter(x[:, 0], x[:, 1], c=y)
    # Set the title and the axis labels
    ax.set(title=title, xlabel=x_label, ylabel=y_label)
    _ = ax.legend(
        scatter.legend_elements()[0], classes, loc="lower right", title="Classes"
    )

    # ax.set_title(title)
    # ax.set_xlabel('Component 1')
    # ax.set_ylabel('Component 2')
    # Show the plot
    plt.show()
    return

# Load iris datasets
iris = load_iris()
iris_data = iris.data
iris_labels = iris.target
iris_target_names = iris.target_names   # Get the names of the classes
# Perform PCA on the iris dataset
pca = PCA(n_components=2)
iris_data_pca = pca.fit_transform(iris_data)
# Plot the reduced data using PCA anf t-SNE
scatter_plot_data(iris_data_pca, iris_labels, iris_target_names, 
                  title="PCA on Iris Dataset",
                  x_label="Principal Component 1",
                  y_label="Principal Component 2"            
                  )
# Perform t-SNE on the iris dataset
tsne = TSNE(n_components=2, perplexity=30, learning_rate=200, n_iter=1000)
iris_data_tsne = tsne.fit_transform(iris_data)
scatter_plot_data(iris_data_tsne, iris_labels, iris_target_names, 
                  title="t-SNE on Iris Dataset",
                  x_label="Principal Component 1",
                  y_label="Principal Component 2"            
                  )
```

다음 플롯은 iris 데이터 세트에 대한 PCA와 t-SNE의 결과를 보여준다. 두 기법 모두 iris 꽃의 세 가지 클래스를 분리할 수 있음을 알 수 있지만, t-SNE는 PCA보다 더 콤팩트하고 명확한 클러스터링을 생성하는 것으로 보인다.

![](./images/pca_iris.png)

![](./images/t-sne_iris.png)

## <a name="summary"></a> 마치며
이 포스팅에서는 PCA(Principle Component Analysis)와 t-분산 확률적 이웃 임베딩(t-SNE)이라는 두 기법을 사용하여 데이터의 차원을 줄이는 방법을 설명하였다. 또한 다른 데이터세트에 대한 PCA와 t-SNE의 결과를 비교하고 각 기법의 장단점을 보였다.

다음은 기억해야 할 몇 가지 핵심 사항이다.

- 차원 축소는 관련 정보를 최대한 보존하면서 고차원 데이터를 저차원 표현으로 변환하는 과정이다.
- 차원 축소를 통해 데이터를 보다 쉽고 효율적으로 시각화, 계산 및 해석할 수 있다.
- PCA는 데이터에서 최대 분산의 방향을 찾고 데이터를 저차원 부분 공간에 투영하려는 선형 기법이다.
- t-SNE는 고차원 공간에서 유사한 데이터 포인트의 클러스터를 찾아내고, 데이터의 로컬 구조를 보존하면서 저차원 공간에 매핑하려고 하는 비선형 기법이다.
- PCA와 t-SNE는 서로 다른 강점과 약점을 가지고 있으며, 기법의 선택은 데이터 분석의 특성과 목적에 따라 달라진다.

> **시연 completed**
