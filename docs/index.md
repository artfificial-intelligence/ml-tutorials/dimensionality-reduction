# 차원 축소: PCA와 t-SNE <sup>[1](#footnote_1)</sup>

> <font size="3">PCA와 t-SNE 기법을 사용하여 데이터의 차원을 줄이는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./dimensionality-reduction.md#intro)
1. [차원 축소란 무엇이며 왜 중요할까?](./dimensionality-reduction.md#sec_02)
1. [주성분 분석(PCA)](./dimensionality-reduction.md#sec_03)
1. [t-분산 확률적 이웃 임베딩(t-SNE)](./dimensionality-reduction.md#sec_04)
1. [데이터세트에서 PCA와 t-SNE의 비교](./dimensionality-reduction.md#sec_05)
1. [마치며](./dimensionality-reduction.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 15 — Dimensionality Reduction: PCA and t-SNE](https://ai.plainenglish.io/ml-tutorial-15-dimensionality-reduction-pca-and-t-sne-3ff26ca255b0?sk=423a9d73833747e83a827d0d27099c53)를 편역한 것이다.
